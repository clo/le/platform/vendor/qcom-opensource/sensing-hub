// @file sns_physical_sensor_test.proto
//
// @brief Defines Test API message types for Physical Sensors.
//
// @copyright 
// Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear
//
// @details All physical Sensor drivers are required to use this API to support
// self-test. SNS_PHYSICAL_SENSOR_TEST_TYPE_COM is a mandatory test type and must be
// implemented in all physical Sensor drivers. Any new or device-specific
// test type may be defined in the Sensor-specific API.
//
// ## Processing Stream Requests:
// 1. The sns_physical_sensor_test_config message starts the specified test.
//    a. If the associated physical sensor has existing client requests, the
//       Sensor shall:
//          1. Pause the existing client requests
//          2. Execute the self-test request to completion
//          3. Resume the client requests
//    b. If a test is running when a new client request is received, the Sensor
//       shall:
//          1. Reject the client request
//          2. Continues executing the self-test request to completion
//
// ## Publishing Stream Events:
// 1. The Sensor publishes a sns_physical_sensor_test_event upon completion of
//    a test.

syntax = "proto2";
import "nanopb.proto";


// @brief Physical Sensor test Message IDs
enum sns_physical_sensor_test_msgid
{
  option (nanopb_enumopt).long_names = false;

  // @brief Test config request to a physical Sensor
  // Message: sns_physical_sensor_test_config
  SNS_PHYSICAL_SENSOR_TEST_MSGID_SNS_PHYSICAL_SENSOR_TEST_CONFIG = 515;

  // @brief Test event message from a physical Sensor
  // Message: sns_physical_sensor_test_event
  SNS_PHYSICAL_SENSOR_TEST_MSGID_SNS_PHYSICAL_SENSOR_TEST_EVENT  = 1026;
}

// @brief Supported test types for physical sensors
enum sns_physical_sensor_test_type
{
  option (nanopb_enumopt).long_names = false;

  // @brief Software test.
  SNS_PHYSICAL_SENSOR_TEST_TYPE_SW = 0;

  // @brief Sensor Hardware test.
  SNS_PHYSICAL_SENSOR_TEST_TYPE_HW = 1;

  // @brief Factory test used for Sensor calibration.
  SNS_PHYSICAL_SENSOR_TEST_TYPE_FACTORY = 2;

  // @brief Communication bus test.
  SNS_PHYSICAL_SENSOR_TEST_TYPE_COM = 3;
}

// @brief Physical Sensor test configuration request
message sns_physical_sensor_test_config
{
  // @brief Requested test type.
  required sns_physical_sensor_test_type test_type = 1;
}

// @brief Physical Sensor test event
message sns_physical_sensor_test_event
{
  // @brief Result if the test execution was successful:
  // true for success
  // false for failure
  required bool test_passed = 1 [default = true];

  // @brief test_type which was completed
  required sns_physical_sensor_test_type test_type = 2 [default = SNS_PHYSICAL_SENSOR_TEST_TYPE_COM];

  // @brief Driver specific test data. This field may be used to pass additional
  // information (such as failure codes, debug data, etc).
  optional bytes test_data = 3;
}
